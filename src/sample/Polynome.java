package sample;

import java.util.LinkedList;

public class Polynome {

    LinkedList<Monome> terms;
    int degree;

    public Polynome() {
        terms = new LinkedList<Monome>();
        degree = -1;
    }

    private void updateDegree()
    {
        int tempDegree = -1;

        for(Monome i:terms)
        {
            if(i.getCoeff() != 0 && i.getPower() > tempDegree)
                tempDegree = i.getPower();
        }

        degree = tempDegree;
    }

    private int getPositionToAdd(Monome x)
    {
        boolean added = false;
        int indexToAdd = -2;

        for (Monome i:terms) {
            if (i.getPower() == x.getPower()) {
                i.setCoeff(i.getCoeff() + x.getCoeff());
                indexToAdd = -1;
                break;
            }
            if (i.getPower() < x.getPower()) {
                indexToAdd = terms.indexOf(i);
                break;
            }
        }

        if(indexToAdd == -2)
            indexToAdd = terms.size();

        return indexToAdd;
    }

    public void addElement(Monome x) {

        int indexToAdd = getPositionToAdd(x);
        if(indexToAdd != -1)
            terms.add(indexToAdd, x);

        LinkedList<Monome> toRemove = new LinkedList<Monome>();

        for(Monome i:terms)
            if(i.getCoeff() == 0)
                toRemove.add(i);

        if(terms.size() > toRemove.size())
            terms.removeAll(toRemove);

        updateDegree();
    }

    @Override
    public String toString() {
        String s = new String();

        s = terms.getFirst().toString();

        for(Monome i:terms) {
            if (i != terms.getFirst()) {
                if(i.getCoeff() != 0) {
                    if (i.getCoeff() > 0) {
                        s = s + "+";
                    }

                    s = s + i.toString();
                }
            }
        }

        return s;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }
}
