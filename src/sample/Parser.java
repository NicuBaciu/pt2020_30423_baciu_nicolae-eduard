package sample;

import java.util.LinkedList;

public class Parser {

    public Monome toMonome(String input)
    {
        Monome m = new Monome();
        if(!input.contains("x"))
        {
            if(!input.matches("[0-9]+[.]?[0-9]?"))
                return null;
            m.setCoeff(Double.valueOf(input));
            m.setPower(0);
            return m;
        }
        String[] var = input.split("x");
        if(var[0].equals(""))
            m.setCoeff(1.0);
        else
        {
            if(var[0].endsWith("*"))
            {
                var[0] = var[0].substring(0, var[0].length() - 1);
            }
            if(var[0].matches("-"))
                m.setCoeff(-1);
            else
            {
                if(!var[0].matches("[0-9]+[.]?[0-9]?"))
                    return null;
                m.setCoeff(Double.valueOf(var[0]));
            }

        }
        if(var.length > 1)
        {
            if(var[1].equals(""))
                m.setPower(1);
            else
            {
                var[1] = var[1].substring(1);

                if(!var[1].matches("[0-9]+"))
                    return null;
                m.setPower(Integer.valueOf(var[1]));
            }
        }
        else if(input.contains("x"))
            m.setPower(1);
        return m;
    }

    public Polynome toPolynome(String input)
    {
        LinkedList<Monome> monomes = new LinkedList<>();
        Polynome p = new Polynome();

        String temp = input.replaceAll("-","+-");

        String[] monomeStrings = temp.split("\\+");

        for(String i:monomeStrings)
        {
            Monome m = toMonome(i);

            if(m == null)
                return null;
            p.addElement(toMonome(i));
        }

        return p;
    }
}
