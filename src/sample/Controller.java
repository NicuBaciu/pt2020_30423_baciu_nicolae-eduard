package sample;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint;

import java.util.LinkedList;

public class Controller implements EventHandler {

    Operations operations;
    Parser parser;

    public Label resLabel, remainderLabel;
    public TextField xText, yText;
    public Button add, sub, mult, div, diff, integ;

    public Controller(Button add, Button sub, Button mult, Button div, Button diff, Button integ,
                      TextField xText, TextField yText, Label resLabel, Label remainderLabel)
    {
        this.add = add;
        this.sub = sub;
        this.mult = mult;
        this.div = div;
        this.diff = diff;
        this.integ = integ;
        this.xText = xText;
        this.yText = yText;
        this.resLabel = resLabel;
        this.remainderLabel = remainderLabel;

        operations = new Operations();
        parser = new Parser();
    }

    @Override
    public void handle(Event event) {
        Polynome tempX = parser.toPolynome(xText.getText());
        Polynome tempY = parser.toPolynome(yText.getText());

        if(tempX == null)
        {
            xText.setStyle("-fx-control-inner-background: red");
            return;
        }
        else
            xText.setStyle("-fx-control-inner-background: white");

        if(tempY == null)
        {
            yText.setStyle("-fx-control-inner-background: red");
            return;
        }
        else
            yText.setStyle("-fx-control-inner-background: white");



        if (event.getSource() == add) {
            resLabel.setText(operations.add(tempX, tempY).toString());

            remainderLabel.setVisible(false);
        }
        if(event.getSource() == sub) {
            resLabel.setText(
                    operations.subtract(tempX, tempY).toString());
            remainderLabel.setVisible(false);
        }
        if(event.getSource() == mult) {
            resLabel.setText(
                    operations.multiply(tempX, tempY).toString());
            remainderLabel.setVisible(false);
        }
        if(event.getSource() == div)
        {
            LinkedList<Polynome> resultOfDiv;
            resultOfDiv = operations.divide(tempX, tempY);

            resLabel.setText("Quotient: " + resultOfDiv.getFirst().toString());
            remainderLabel.setVisible(true);
            if(resultOfDiv.getLast().toString() != "")
                remainderLabel.setText("Remainder: " + resultOfDiv.getLast().toString());
            else
                remainderLabel.setText("Remainder: 0");
        }
        if(event.getSource() == diff) {
            if(tempX.degree > 0)
            {
                resLabel.setText(operations.differentiate(tempX).toString());
                remainderLabel.setVisible(false);
            }
        }
        if(event.getSource() == integ) {
            resLabel.setText(operations.integrate(tempX).toString());
            remainderLabel.setVisible(false);
        }
    }
}
