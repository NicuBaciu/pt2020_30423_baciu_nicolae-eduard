package sample;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class Tester {
    @Test
    public void Test()
    {
        List<Monome> a = new LinkedList<Monome>();
        List<Monome> b = new LinkedList<Monome>();

        a.add(new Monome(3, 2));
        a.add(new Monome(2, 3));
        a.add(new Monome(1, -1));
        a.add(new Monome(0, 5));

        b.add(new Monome(2, 1));
        b.add(new Monome(1, -1));
        b.add(new Monome(0, 1));

        Polynome p = new Polynome();

        for(Monome i:a)
        {
            p.addElement(i);
        }

        Polynome q = new Polynome();

        for(Monome i:b)
        {
            q.addElement(i);
        }

        Operations op = new Operations();
        Parser parser = new Parser();

        Assert.assertEquals(p.toString(), parser.toPolynome("2.0x^3+3.0*x^2-x+5.0").toString());
        Assert.assertEquals(q.toString(), parser.toPolynome("x^2-x+1.0").toString());

        Assert.assertEquals("2.0*x^3+3.0*x^2-x+5.0", p.toString());
        Assert.assertEquals("x^2-x+1.0", q.toString());

        Assert.assertEquals("2.0*x^3+4.0*x^2-2.0*x+6.0", op.add(p, q).toString());
        Assert.assertEquals("2.0*x^3+2.0*x^2+4.0", op.subtract(p, q).toString());
        Assert.assertEquals("2.0*x^5+x^4-2.0*x^3+9.0*x^2-6.0*x+5.0", op.multiply(p, q).toString());
        Assert.assertEquals("6.0*x^2+6.0*x-1.0", op.differentiate(p).toString());
        Assert.assertEquals("0.5*x^4+x^3-0.5*x^2+5.0*x", op.integrate(p).toString());

        LinkedList<Polynome> result = new LinkedList<>();

        result = op.divide(p, q);

        Assert.assertEquals("2.0*x+5.0", result.get(0).toString());
        Assert.assertEquals("2.0*x", result.get(1).toString());
    }

}
