package sample;

import java.util.LinkedList;

public class Operations {

    public Polynome add(Polynome x, Polynome y)
    {
        Polynome result = new Polynome();

        for(Monome i:x.terms)
        {
            result.addElement(new Monome(i.getPower(), i.getCoeff()));
        }

        for(Monome i:y.terms)
        {
            result.addElement(new Monome(i.getPower(), i.getCoeff()));
        }

        return result;
    }

    public Polynome subtract(Polynome x, Polynome y)
    {
        Polynome result = new Polynome();

        for(Monome i:x.terms)
        {
            result.addElement(new Monome(i.getPower(), i.getCoeff()));
        }

        for(Monome i:y.terms)
        {
            result.addElement(new Monome(i.getPower(), -i.getCoeff()));
        }

        return result;
    }

    public Polynome multiply(Polynome x, Polynome y)
    {
        Polynome result = new Polynome();

        for(Monome i:x.terms)
        {
            for(Monome j:y.terms)
            {
                Monome temp = new Monome();
                temp.setCoeff(i.getCoeff() * j.getCoeff());
                temp.setPower(i.getPower() + j.getPower());

                result.addElement(temp);
            }
        }

        return result;
    }

    public LinkedList<Polynome> divide(Polynome x, Polynome y)
    {
        LinkedList<Polynome> results = new LinkedList<>();

        Polynome quotient = new Polynome();
        Polynome remainder = new Polynome();

        while(x.getDegree() >= y.getDegree())
        {
            Monome a = x.terms.getFirst();
            Monome b = y.terms.getFirst();
            Monome tempRes = new Monome();

            tempRes.setPower(a.getPower() - b.getPower());
            tempRes.setCoeff(a.getCoeff() / b.getCoeff());

            quotient.addElement(tempRes);

            Polynome temp = new Polynome();
            temp.addElement(tempRes);

            remainder = multiply(y, temp);
            remainder = subtract(x, remainder);
            x = remainder;
        }

        results.add(quotient);
        results.add(remainder);

        return results;
    }

    public Polynome differentiate(Polynome x)
    {
        Polynome result = new Polynome();

        for(Monome i:x.terms)
        {
            if(i.getPower() > 0)
            {
                result.addElement(new Monome(i.getPower() - 1, i.getCoeff() * (i.getPower())));
            }
        }

        return result;
    }

    public Polynome integrate(Polynome x)
    {
        Polynome result = new Polynome();

        for(Monome i:x.terms)
        {
            result.addElement(new Monome(i.getPower() + 1, i.getCoeff() / (i.getPower() + 1)));
        }

        return result;
    }
}
