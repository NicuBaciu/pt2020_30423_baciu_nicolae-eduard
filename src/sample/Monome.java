package sample;

public class Monome {
    int power;
    double coeff;

    public Monome() {
        this.power = 0;
        this.coeff = 1;
    }

    public Monome(int power, double coeff) {
        this.power = power;
        this.coeff = coeff;
    }

    @Override
    public String toString() {
        if(power > 0 && power != 1)
        {
            if(coeff != 0)
            {
                if(coeff != 1 && coeff != -1)
                    return String.valueOf(coeff) + "*x^" + String.valueOf(power);
                else if(coeff == 1)
                    return "x^" + String.valueOf(power);
                else if(coeff == -1)
                    return "-x^" + String.valueOf(power);
                else
                    return "";
            }
            else
                return "";
        }
        else if(power == 1)
        {
            if(coeff != 0)
            {
                if(coeff != 1 && coeff != -1)
                    return String.valueOf(coeff) + "*x";
                else if(coeff == 1)
                    return "x";
                else if(coeff == -1)
                    return "-x";
                else
                    return "";
            }
            else
                return "";
        }
        else
        {
            if(coeff != 0)
                return String.valueOf(coeff);
            else
                return "";
        }
    }

    public int getPower() {
        return power;
    }

    public double getCoeff() {
        return coeff;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setCoeff(double coeff) {
        this.coeff = coeff;
    }
}
