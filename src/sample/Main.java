package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    View view;

    public Main()
    {
        view = new View();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Polynome calculator");
        primaryStage.setScene(new Scene(view.getParent(), 350, 265));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

        Monome x = new Monome(2, 1);
        Polynome p = new Polynome();

        p.addElement(x);

        System.out.println(p);
    }
}
