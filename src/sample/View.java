package sample;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;

public class View {

    Controller controller;

    Font bA14, bA16;
    Label xLabel, yLabel, resultLabel;
    public Label resLabel, remainderLabel;
    public TextField xText, yText;
    public Button add, sub, mult, div, diff, integ;

    Separator[] separators;

    public View()
    {
        bA14 = new Font("Book Antiqua", 16);
        bA16 = new Font("Book Antoqua", 14);

        xLabel = new Label();
        xLabel.setText(" x");
        xLabel.setFont(bA14);

        yLabel = new Label();
        yLabel.setText(" y");
        yLabel.setFont(bA14);

        resultLabel = new Label();
        resultLabel.setText("Result:");
        resultLabel.setFont(bA14);
        resultLabel.setPrefWidth(200);

        xText = new TextField();
        xText.setPrefSize(346, 25);
        xText.setPadding(new Insets(0, 0, 0, 5));
        yText = new TextField();
        yText.setPrefSize(346, 25);
        yText.setPadding(new Insets(0, 0, 0, 5));

        resLabel = new Label("");
        resLabel.setFont(bA16);
        resLabel.setPrefSize(350, 30);

        remainderLabel = new Label("");
        remainderLabel.setFont(bA16);
        remainderLabel.setPrefSize(350, 30);

        add = new Button();
        add.setText("+");
        add.setPrefWidth(57);

        sub = new Button();
        sub.setText("-");
        sub.setPrefWidth(57);

        mult = new Button();
        mult.setText("*");
        mult.setPrefWidth(57);

        div = new Button();
        div.setText("/");
        div.setPrefWidth(57);

        diff = new Button();
        diff.setText("diff");
        diff.setPrefWidth(57);

        integ = new Button();
        integ.setText("integ");
        integ.setPrefWidth(57);

        separators = new Separator[3];

        separators[0] = new Separator();
        separators[1] = new Separator();
        separators[2] = new Separator();

        for(Separator i:separators) {
            i.setOrientation(Orientation.HORIZONTAL);
            i.setPrefSize(350, 20);
        }

        controller = new Controller(add, sub, mult, div, diff, integ , xText, yText, resLabel, remainderLabel);
    }

    public Parent getParent()
    {
        FlowPane root = new FlowPane();

        root.getChildren().add(xLabel);
        root.getChildren().add(xText);

        root.getChildren().add(separators[0]);

        root.getChildren().add(yLabel);
        root.getChildren().add(yText);

        root.getChildren().add(separators[1]);

        root.getChildren().add(add);
        root.getChildren().add(sub);
        root.getChildren().add(mult);
        root.getChildren().add(div);
        root.getChildren().add(diff);
        root.getChildren().add(integ);

        root.getChildren().add(separators[2]);

        root.getChildren().add(resultLabel);
        root.getChildren().add(resLabel);
        root.getChildren().add(remainderLabel);

        add.setOnMouseClicked(controller);
        sub.setOnMouseClicked(controller);
        mult.setOnMouseClicked(controller);
        div.setOnMouseClicked(controller);
        diff.setOnMouseClicked(controller);
        integ.setOnMouseClicked(controller);

        return root;
    }
}
